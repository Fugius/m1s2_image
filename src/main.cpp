#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "algorithm"
#include <string>

#include "SobelFunc.h"
#include "Histogram.h"

Histogram preprocessHisto(Histogram h1);
int countSteps(Histogram h1);

int main() {
    vector<int> steps;

    //changer 7 pour TestImages, 8 pour BDD
    for (int i = 1; i <= 7; i++) {
        int ins = i;
        Mat img;
        string path = "C:/Users/dball/Desktop/M1 VMI/S2/Image/TestImages/" + to_string(i) + ".jpg";
        img = imread(path, IMREAD_COLOR);
        
        //initialisation des paramettres pour la changement de taille
        int steps_l = 0;
        int alpha = 380;
        float factor = alpha / (float)img.rows;

        //
        Size s((int)img.cols * factor, alpha);
        
        //changement de la taille de l'image proportionnelle a alpha
        resize(img, img, s);
        //cvtColor(img, img, COLOR_BGR2GRAY);
        img = SobelL(img);
        //threshold(img, img, 127, 255, THRESH_BINARY);

        //

        Histogram h1 = Histogram::calcAccHistoH(img, 5);

        //numero de l'image dont on veux voir les resultats
        int inpectImage = 5;

        if (i == inpectImage) {
            h1.showHistogram("histo " + to_string(i));
            namedWindow("sobel " + to_string(i), WINDOW_AUTOSIZE);
            imshow("sobel " + to_string(i), img);
        }

        //debug ca 
        vector<double> vecRes = {};
        int part = 4;

        for (int i = 0; i < part; i++) 
        {
            //recuperation du histogramme de l'image
            vector<double> histogram = h1.getHistogram();
            //
            vector<double> temp(histogram.begin() + (int)(i * histogram.size() / part), histogram.begin() + (int)((i + 1) * histogram.size() / part) - 1);
            
            //cout << "" << temp.size() << endl;;

            //
            Histogram tempHistogram(temp);
            tempHistogram = preprocessHisto(tempHistogram);

            histogram = tempHistogram.getHistogram();
            //steps_l += countSteps(tempHistogram);
            vecRes.insert(vecRes.end(), histogram.begin(), histogram.end());
        }

        h1.setHistogram(vecRes);
        steps_l += countSteps(h1);

        //afficher le histogramme apres le traitement
        if (i == ins) {
            h1.showHistogram("histo_processed " + to_string(i));
        }

        steps.push_back(steps_l);
        countSteps(h1);
    }

    //afficher le numero des marches pour chaque image
    for (int i = 0; i < steps.size(); i++)
        cout << i + 1 << ".jpg : " << steps[i] << " steps !" << endl;

    waitKey(0);
    return 0;
}