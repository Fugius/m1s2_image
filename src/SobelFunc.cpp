#include "SobelFunc.h"

/**
*Realise la convolution d'une image
*
*@param img
*@param filter filtre sous forme de matrice 
*
*@return res: 
*/
Mat Conv2D(Mat img, vector<vector<float>> filter) {
    //
    int size = filter.size();
    //
    int start = size / 2;

    //
    Mat res(img.rows - 2 * start, img.cols - 2 * start, CV_32F);

    //
    for (int i = start; i < img.cols - start; i++) {
        for (int j = start; j < img.rows - start; j++) {

            float pixel_value = 0;
            for (int m = 0; m < size; m++) {
                for (int n = 0; n < size; n++) {
                    int val = (int)img.at<uchar>(j - start + n, i - start + m);
                    pixel_value += ((float)val * (float)filter[n][m]);
                }
            }
            //
            res.at<float>(j - start, i - start) = (pixel_value);
        }
    }

    return res;
}

/**
*..
*
*@param img
*
*@return p3
*/
Mat SobelL(Mat img) {
    //cvtColor(img, img, COLOR_BGR2GRAY);
    cvtColor(img, img, COLOR_BGR2GRAY);

    //Kernel 3x3
    vector<vector<float>> filtre1 = { {-1, 0, 1},
                                    {-2, 0, 2},
                                    {-1, 0, 1} };


    vector<vector<float>> filtre2 = { {-1, -2, -1},
                                    {0, 0, 0},
                                    {1, 2, 1} };
 
    //convolution de l'image
    Mat p1 = Conv2D(img, filtre1);
    Mat p2 = Conv2D(img, filtre2);

    Mat p3 = p2;

    //g = sqrt g1^2 + g2^2
    for (int i = 0; i < p3.cols; i++) {
        for (int j = 0; j < p3.rows; j++) {
            p3.at<float>(j, i) = sqrt(p1.at<float>(j, i) * p1.at<float>(j, i) + p2.at<float>(j, i) * p2.at<float>(j, i));
        }
    }

    //normalization des valeurs des images
    normalize(p3, p3, 255, 0, NORM_MINMAX);

    //conversion de l'image en ddepth 8UC1
    p3.convertTo(p3, CV_8UC1);

    //otsu binarization
    threshold(p3, p3, 0, 255, THRESH_BINARY | THRESH_OTSU);

    return p3;
}

/*
*differentes methodes de openCV utilisees pour faire des tests
*
*@param img: image a etre traite 
*
*@return 
*/
Mat SobelOCV(Mat img) 
{
    //GaussianBlur(img, img, Size(3, 3), 0, 0, BORDER_DEFAULT);
    // Convert the image to grayscale
    //cvtColor(img, img, COLOR_BGR2GRAY);
    
    Mat grad;
    //images ou on sauvegarde les resultats des filtre de sobel selon le gradient 
    Mat grad_x, grad_y;
    Mat abs_grad_x, abs_grad_y;

    Sobel(img, grad_x, -1, 1, 0);
    Sobel(img, grad_y, -1, 0, 1);
    
    // converting back to CV_8U
    convertScaleAbs(grad_x, abs_grad_x);
    convertScaleAbs(grad_y, abs_grad_y);
    
    //creation du Mat final 
    //somme des deux images a gradient differents
    addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad);
   
    return grad;

    //filtre de sobel simple
    //Sobel(img, imgRes, -1, 1, 0);
    
    //utilisation de la methode Canny pour la detection des countours
    /*blur(img, img, Size(3, 3));
    Canny(img, img, 100, 300, 3);
    imshow("Sobel Image", img);
    waitKey();
    return imgRes;*/
}
