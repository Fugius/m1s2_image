#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <string>

#include <vector>
#include <iostream>

#include <cmath>
#include <algorithm>

using namespace cv;
using namespace std;

class Histogram {
private:
    vector<double> histo;

public:
    Histogram();
    Histogram(vector<double> histo);

    void setHistogram(vector<double> hsito);
    vector<double> getHistogram();

    static Histogram calcAccHistoH(Mat img, int bin_s);
    static Histogram calcAccHistoV(Mat img, int bin_s);

    void showHistogram(string n);

    Histogram preprocessHisto(Histogram h1);
    int countSteps(Histogram h1);

};
