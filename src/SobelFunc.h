#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <random>
#include <vector>
#include <iostream>

#include <cmath>

using namespace cv;
using namespace std;

Mat Conv2D(Mat img, vector<vector<float>> filter);
Mat SobelOCV(Mat img);
Mat SobelL(Mat img);
