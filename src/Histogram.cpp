#include "Histogram.h"

Histogram::Histogram() {};

Histogram::Histogram(vector<double> histo) {
    this->histo = histo;
}

vector<double> Histogram::getHistogram() {
    return this->histo;
}

void Histogram::setHistogram(vector<double> histo) {
    this->histo = histo;
}

/**
*affiche l'histogramme a partir de son nom 
*
*@param name: nom de l'image dont on affiche l'histogramme
*/
void Histogram::showHistogram(string name) {
    if (histo.size() == 0) return;

    //on affiche l'histogramme
    srand((unsigned)time(NULL));

    //creation de l'image qui va contenir l'histogramme
    Mat img_histo(300, 700, CV_8UC3, Scalar(0, 0, 0));
    float lbarre = ((float)img_histo.cols / (float)histo.size());

    //
    float max_ = *max_element(histo.begin(), histo.end());
    float min_ = *min_element(histo.begin(), histo.end());

    //
    for (int i = 0; i < histo.size(); i++) {
        if (min_ < max_) { histo[i] += -min_; }
        histo[i] = (histo[i] / max_) * (img_histo.rows - 3);
    }

    //
    for (int i = 0; i < histo.size(); i++) {
        rectangle(img_histo, Point(lbarre * (i), img_histo.rows - (int)histo[i]),
            Point(lbarre * (i + 1), img_histo.rows),
            Scalar((int)(rand() % 230 + 25), (int)(rand() % 230 + 25), (int)(rand() % 230 + 25)), -1);
    }
    //fenetre d'affiche de l'hitogramme
    namedWindow("Histo " + name, WINDOW_AUTOSIZE);
    imshow("Histo " + name, img_histo);
}

/**
*calcule l'histogramme de projection verticale
*
*@param img: image dont on veut calculer l'histogramme
*@param bin_s: 
*
*@return result
*/
Histogram Histogram::calcAccHistoV(Mat img, int bin_s) {
    //vecteur contenant l'histogramme
    vector<double> h;

    //
    int rest = img.cols % bin_s;
    //
    for (int i = 0; i < img.cols - rest; i += bin_s) {
        double col = 0;
        for (int k = 0; k < bin_s; k++) {
            for (int j = 0; j < img.rows; j++) {
                //
                int pixel_value = (int)(img.at<uchar>(j, i + k));
                if (pixel_value > 0) col += 1;
            }
        }
        h.push_back(col);
    }

    //
    double c = 0;
    for (int i = img.cols - rest; i < rest; i++) {
        for (int j = 0; j < img.rows; j++) {
            int pixel_value = (int)(img.at<uchar>(j, i));
            if (pixel_value > 0) c += 1;
        }
    }
    h.push_back(c);

    //initialisation de l'objet histogramme 
    Histogram result(h);
    return result;
}

/**
*calcule l'histogramme de projection horizontale
*
*@param img: image dont on veut calculer l'histogramme
*@param bin_s:
*
*@return result
*/
Histogram Histogram::calcAccHistoH(Mat img, int bin_s) {
    //vecteur contenant l'histogramme
    vector<double> h;

    //
    int rest = img.rows % bin_s;
    //
    for (int i = 0; i < img.rows - rest; i += bin_s) {
        double row = 0;
        for (int k = 0; k < bin_s; k++) {
            for (int j = 0; j < img.cols; j++) {
                //
                int pixel_value = (int)(img.at<uchar>(i + k, j));
                if (pixel_value > 0) row += 1;
            }
        }
        h.push_back(row);
    }

    //
    double r = 0;
    for (int i = img.rows - rest; i < img.rows; i++) {
        for (int j = 0; j < img.cols; j++) {
            int pixel_value = (int)(img.at<uchar>(i, j));
            if (pixel_value > 0) r += 1;
        }
    }
    h.push_back(r);

    //initialisation de l'objet histogramme 
    Histogram result(h);
    return result;
}

/**
*compte les pics pour en deduir les marches
*
*@param h1: histogramme dont on veut compter les marches 
*
*@return count 
*/
int countSteps(Histogram h1) {
    //calc espace entre pics moyen
    vector<double> h = h1.getHistogram();

    //traitement de l'histogramme 
    //remplir l'espace vide entre deux pics 
    for (int i = 0; i < h.size() - 3; i++)
    {
        //si il y a UN espace entre 2 pics, on le remplit
        if (h[i] != 0 && h[i] != 10 && h[i + 1] == 0 && h[i + 2] != 0 && h[i + 2] != 10)
            h[i + 1] = h[i];
    }

    //compter les marches 
    int count = 0;
    bool pass = false;
    //parcourir l'histogramme
    for (int i = 0; i < h.size(); i++)
    {
        double b = h[i];

        //compter le pic (b != 0) si ils est pas precede d'un pic 
        if (b != 0 && pass == false)
        {
            count++;
            pass = true;
        }
        //remettre pass a false pour pouvoir compter les autres pics(marches)
        else if (b == 0 && pass == true)
        {
            pass = false;
        }
    }
    return count;
}

/**
*pre-traitement de l'histogramme pour recuperer que les pics qui correspont au marches
*
*@param h1: histogramme dont on veut compter les marches
*
*@return h1
*/
Histogram preprocessHisto(Histogram h1) {

    vector<double> h = h1.getHistogram();

    //3eme quartile
    sort(h.begin(), h.end());
    double q3 = h[(int)(h.size() * 0.75)];


    h = h1.getHistogram();
    for (int i = 0; i < h.size(); i++) {
        if (h[i] <= q3) h[i] = 0;
    }
    h1.setHistogram(h);

    //moyenne pour eliminer les toutes petites valeurs
    h = h1.getHistogram();
    double mean = 0;
    for (auto& b : h) {
        mean += b;
    }
    mean /= h.size();

    for (int i = 0; i < h.size(); i++) {
        if (h[i] <= mean) h[i] = 0;
    }
    h1.setHistogram(h);

    return h1;
}

